# Jenkins Compose

Docker-compose project starting jenkins main and additional workers for jenkins course lab

## Pre reqs:
- docker or alike installed
- docker-compose or alike installed

# Instructions

- run `docker-compose up -d` to start containers with jenkins master and worker
- to connect master with worker, use private cert provided in repo.
- 